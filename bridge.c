#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <time.h>

// Define constants
#define  MAX_WORKERS 24
#define  TIME_TO_CROSS_BRIDGE 50
#define  MAX_CARS_ON_BRIDGE 5

// Semaphores for cars on bridge
sem_t n;
sem_t s;

// Semaphores for locks and signals
sem_t check;
sem_t done;
sem_t lock;
sem_t queue;

// Vars
int cars;
int carsOnBridge[MAX_CARS_ON_BRIDGE];
int nextCar = 0;
int lastCar = 0;


void * car(void *);
void wait();

void createCars(int count) {
  pthread_t carid[count];
  long w;
  for (w = 0; w < count; w++)
    pthread_create(&carid[w], NULL, car, (void *) w);
}

// No need for semaphores to lock this because
// it's only called when semaphore is locked.
void queueRemove() {
  nextCar++;
  nextCar = nextCar % 5;
}

// No need for semaphores to lock this because
// it's only called when semaphore is locked.
void queueAdd() {
  lastCar++;
  lastCar = lastCar % 5;
}

void south(int id, int trips) {
  int value, svalue, nvalue, sleep = ((rand()%9) + 1) * 1000;
  sem_wait(&check);
  sem_getvalue(&n, &value);
  // Check if bridge is empty
  if (value == MAX_CARS_ON_BRIDGE) {
    sem_wait(&s);

    // Add car to queue on bridge
    carsOnBridge[lastCar] = id;
    queueAdd();

    sem_post(&check);
    printf("CAR %d, CROSSING BRIDGE SOUTHBOUND FOR %dms\n", id, sleep/1000);
    usleep(sleep); // Travel

    // Wait to exit
    while (1) {
      sem_wait(&queue);
      // Check if it's your turn to exit
      if (id == carsOnBridge[nextCar]) {
        queueRemove();
        sem_post(&queue);
        break; // Exit
      }
      sem_post(&queue);
      usleep(1000);
    }

    trips--;
    sem_post(&s);
    wait(1, id, trips);
  }
  else {
    // Bridge not empty, go back to waiting
    sem_post(&check);
    wait(0, id, trips);
  }
}

void north(int id, int trips) {
  int value, sleep = ((rand()%9) + 1) * 1000;
  sem_wait(&check);
  sem_getvalue(&s, &value);
  // Check if bridge is empty
  if (value == MAX_CARS_ON_BRIDGE) {
    sem_wait(&n);

    // Add car to queue on bridge
    carsOnBridge[lastCar] = id;
    queueAdd();

    sem_post(&check);
    printf("CAR %d, CROSSING BRIDGE NORTHBOUND FOR %dms\n", id, sleep/1000);
    usleep(sleep); // Travel

    // Wait to exit
    while (1) {
      sem_wait(&queue);
      // Check if it's your turn to exit
      if (id == carsOnBridge[nextCar]) {
        queueRemove();
        sem_post(&queue);
        break; // Exit
      }
      sem_post(&queue);
      usleep(1000);
    }

    trips--;
    sem_post(&n);
    wait(0, id, trips);
  }
  else {
    // Bridge not empty, go back to waiting
    sem_post(&check);
    wait(1, id, trips);
  }
}

void end(int id) {
  printf("CAR %d TRIP IS DONE!\n", id);
  sem_wait(&lock);
  cars--;
  if (cars == 0)
    sem_post(&done);
  sem_post(&lock);
  pthread_exit(NULL);
}

void wait(int direction, int id, int trips) {
  int sleep = ((rand()%45) + 1) * 1000;
  if (trips == 0)
    end(id);

  if (direction) {
    printf("CAR %d, WAITING %dms TO CROSS TO NORTHBOUND\n", id, sleep/1000);
    usleep(sleep);
    north(id, trips); // 1
  }
  else {
    printf("CAR %d, WAITING %dms TO CROSS TO SOUTHBOUND\n", id, sleep/1000);
    usleep(sleep);
    south(id, trips); // 0
  }
}

int main(int argc, char const *argv[]) {
  srand(time(NULL));
  cars = MAX_WORKERS;

  sem_init(&n, 0, MAX_CARS_ON_BRIDGE);
  sem_init(&s, 0, MAX_CARS_ON_BRIDGE);
  sem_init(&check, 0, 1);
  sem_init(&done, 0, 1);
  sem_init(&lock, 0, 1);
  sem_init(&queue, 0, 1);

  createCars(MAX_WORKERS);

  // Double lock.
  // Lock sempahore and wait for last thread/car to release it.
  sem_wait(&done);
  sem_wait(&done);
  return 0;
}

void * car(void * arg) {
  uintptr_t threadId = (uintptr_t)arg;
  int id = (int) threadId;
  int tripsLeft = ((random() % 3) + 1) * 2;

  wait(threadId%2, id, tripsLeft);
}
